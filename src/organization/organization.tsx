import React from 'react';
import {IRepository, Repository} from "../repository/repository";

export interface IOrganization {
    name: string;
    url: string;
    repository: IRepository;
}

type OrganizationProps = {
    organization: IOrganization;
    errors: any;
    onFetchMoreIssues(): void;
    onStarRepository(repoId: string, viewerHasStarred: boolean): void;
}

export const Organization: React.FunctionComponent<OrganizationProps> = ({organization, errors, onFetchMoreIssues, onStarRepository}) => {
    if (errors) {
        return (
            <p>
                <strong>Something went wrong:</strong>
                {errors.map((error: any) => error.message).join(' ')}
            </p>
        );
    }

    return (
        <div>
            <p>
                <strong>Issues from Organization:</strong>
                <a href={organization.url}>{organization.name}</a>
            </p>
            <Repository repository={organization.repository}
                        onFetchMoreIssues={onFetchMoreIssues}
                        onStarRepository={onStarRepository}/>
        </div>
    );
};
