import React from 'react';
import {IIssue, Issue} from "./issue";

export interface IIssues {
    issues: IIssue[];
    totalCount: number;
    pageInfo: {
        endCursor: string;
        hasNextPage: boolean;
    }
}

type IssuesProps = {
    issues: IIssues
};

export const Issues: React.FunctionComponent<IssuesProps> = ({issues}) => (
    <ul>
        {
            issues.issues.map((issue: IIssue) => (<Issue key={issue.id} issue={issue}/>))
        }
    </ul>
);