import React from 'react';
import {Reactions} from "../reactions/reactions";

export interface IIssue {
    id: string;
    title: string;
    url: string;
    reactions: [];
}

type IssueProps = {
    issue: IIssue;
}

export const Issue: React.FunctionComponent<IssueProps> = ({issue}) => (
    <li>
        <a href={issue.url}>{issue.title}</a>
        <Reactions reactions={issue.reactions}/>
    </li>
);