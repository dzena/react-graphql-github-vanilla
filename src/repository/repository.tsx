import React, {MouseEvent} from 'react';
import {IIssues, Issues} from "../issues/issues";

export interface IRepository {
    id: string;
    name: string;
    url: string;
    viewerHasStarred: boolean;
    issues: IIssues;
    stargazers: {
        totalCount: number;
    };
}

type RepositoryProps = {
    repository: IRepository;
    onFetchMoreIssues(e: MouseEvent<HTMLButtonElement>): void;
    onStarRepository(repoId: string, viewerHasStarred: boolean): void;
};

export const Repository: React.FunctionComponent<RepositoryProps> = ({repository, onFetchMoreIssues, onStarRepository}) => (
    <div>
        <p>
            <strong>In Repository:</strong>
            <a href={repository.url}>{repository.name}</a>
        </p>

        <button type="button" onClick={() => onStarRepository(repository.id, repository.viewerHasStarred)}>
            {repository.stargazers.totalCount}
            &nbsp;
            {repository.viewerHasStarred ? 'Unstar' : 'Star'}
        </button>

        <Issues issues={repository.issues}/>

        <hr/>
        {repository.issues.pageInfo.hasNextPage && (
            <button onClick={onFetchMoreIssues}>More</button>
        )}
    </div>
);
