import React from 'react';
import './App.css';
import {IOrganization, Organization} from "./organization/organization";
import adapter from "./adapter/adapter";
import {serializeAddStarMutation, serializeIssuesQuery, serializeRemoveStarMutation} from "./issues-serializer";

export interface IApp {
    path: string;
    organization: IOrganization;
    errors: any;
}

export const appInitState: IApp = {
    path: 'missive/emoji-mart',
    organization: {
        name: '',
        url: '',
        repository: {
            id: '',
            name: '',
            url: '',
            viewerHasStarred: false,
            stargazers: {
                totalCount: 0
            },
            issues: {
                issues: [],
                pageInfo: {
                    endCursor: '',
                    hasNextPage: false
                },
                totalCount: 0
            }
        }
    },
    errors: null
};

type AppState = Readonly<typeof appInitState>;

class App extends React.Component<IApp, AppState> {
    readonly state: AppState = appInitState;

    componentDidMount() {
        // fetch data
        this._onFetchFromGitHub(this.state.path);
    }

    private _onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        this.setState({path: event.target.value});
    };

    private _onSubmit = (event: React.FormEvent<HTMLFormElement>): void => {
        // fetch data
        this._onFetchFromGitHub(this.state.path);

        event.preventDefault();
    };

    private _onFetchFromGitHub(path: string, cursor?: string): void {
        adapter
            .getIssuesOfRepository(path, cursor)
            .then((result: any) => this.setState(serializeIssuesQuery(result, cursor)));
    }

    private _onFetchMoreIssues(): void {
        const {endCursor} = this.state.organization.repository.issues.pageInfo;

        this._onFetchFromGitHub(this.state.path, endCursor);
    }

    private _onStarRepository = (repoId: string, viewerHasStarred: boolean): void => {
        if (viewerHasStarred) {
            adapter
                .removeStarFromRepository(repoId)
                .then((mutationResult: any) => this.setState(serializeRemoveStarMutation(mutationResult)));
        } else {
            adapter
                .addStarToRepository(repoId)
                .then((mutationResult: any) => this.setState(serializeAddStarMutation(mutationResult)));
        }
    };

    render() {
        const {path, organization, errors} = this.state;

        return (
            <div className="App">
                <h1>React GraphQL GitHub Client</h1>

                <form onSubmit={this._onSubmit}>
                    <label htmlFor="url">
                        Show open issues for https://github.com/
                    </label>
                    <input
                        id="url"
                        type="text"
                        value={path}
                        onChange={this._onChange}
                        style={{width: '300px'}}
                    />
                    <button type="submit">Search</button>
                </form>

                <hr/>

                <Organization organization={organization} errors={errors}
                              onFetchMoreIssues={() => this._onFetchMoreIssues()}
                              onStarRepository={this._onStarRepository}/>
            </div>
        );
    }
}

export default App;
