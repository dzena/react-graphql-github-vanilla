import {IOrganization} from "./organization/organization";
import {IIssue} from "./issues/issue";
import {IReaction} from "./reactions/reaction";
import {IApp} from "./App";

export const serializeIssuesQuery = (queryResult: any, cursor?: string) => (state: IApp): { organization: IOrganization, errors: any } => {
    const {data, errors} = queryResult.data;

    const organization = data.organization;
    let issues = getOrganizationIssues(organization);
    issues.issues.forEach((issue: any) => issue.reactions = getIssueReactions(issue));

    if (!cursor) {
        return {
            organization: {
                ...organization,
                repository: {
                    ...organization.repository,
                    issues
                }
            },
            errors
        };
    }

    const {issues: oldIssues} = state.organization.repository.issues;
    const {issues: newIssues} = issues;
    const updatedIssues = [...oldIssues, ...newIssues];

    return {
        organization: {
            ...organization,
            repository: {
                ...organization.repository,
                issues: {
                    ...issues,
                    issues: updatedIssues,
                },
            },
        },
        errors
    };
};

export const serializeAddStarMutation = (mutationResult: any) => (state: IApp): IApp => {
    const {viewerHasStarred} = mutationResult.data.data.addStar.starrable;
    const {totalCount} = state.organization.repository.stargazers;

    return {
        ...state,
        organization: {
            ...state.organization,
            repository: {
                ...state.organization.repository,
                viewerHasStarred,
                stargazers: {
                    totalCount: totalCount + 1,
                },
            },
        },
    };
};

export const serializeRemoveStarMutation = (mutationResult: any) => (state: IApp): IApp => {
    const {viewerHasStarred} = mutationResult.data.data.removeStar.starrable;
    const {totalCount} = state.organization.repository.stargazers;

    return {
        ...state,
        organization: {
            ...state.organization,
            repository: {
                ...state.organization.repository,
                viewerHasStarred,
                stargazers: {
                    totalCount: totalCount - 1,
                },
            },
        },
    };
};

const getOrganizationIssues = (organization: any): { issues: IIssue[], pageInfo: any, totalCount: number } => (
    {
        issues: organization.repository.issues.edges.map((edge: any) => edge.node),
        pageInfo: organization.repository.issues.pageInfo,
        totalCount: organization.repository.issues.totalCount
    }
);
const getIssueReactions = (issue: any): IReaction[] => issue.reactions.edges.map((r: any) => r.node);
