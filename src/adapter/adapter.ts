import axios from 'axios';
import {ADD_STAR, GET_ISSUES_OF_REPOSITORY, REMOVE_STAR} from "../queries/queries";

const axiosGitHubGraphQL = axios.create({
    baseURL: 'https://api.github.com/graphql',
    headers: {
        Authorization: `bearer ${process.env.REACT_APP_GITHUB_PERSONAL_ACCESS_TOKEN}`
    }
});

// What would be the cons/pros of using a class
export class Adapter {
    public getIssuesOfRepository(path: string, cursor?: string): Promise<any> {
        const [organization, repository] = path.split('/');

        return axiosGitHubGraphQL
            .post('',
                {
                    query: GET_ISSUES_OF_REPOSITORY,
                    variables: {organization, repository, cursor}
                }
            );
    }

    public addStarToRepository(repositoryId: string): Promise<any> {
        return axiosGitHubGraphQL
            .post('',
                {
                    query: ADD_STAR,
                    variables: {repositoryId}
                });
    }

    public removeStarFromRepository(repositoryId: string): Promise<any> {
        return axiosGitHubGraphQL
            .post('',
                {
                    query: REMOVE_STAR,
                    variables: {repositoryId}
                });
    }
}

const adapter = new Adapter();
export default adapter;