import React from 'react';

export interface IReaction {
    id: string;
    content: string;
}

type ReactionProps = {
    reaction: IReaction;
}

export const Reaction: React.FunctionComponent<ReactionProps> = ({reaction}) => (
    <li>
        {reaction.content}
    </li>
);
