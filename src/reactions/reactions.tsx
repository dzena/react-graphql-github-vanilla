import React from 'react';
import {IReaction, Reaction} from "./reaction";

type ReactionsProps = {
    reactions: IReaction[];
}

export const Reactions: React.FunctionComponent<ReactionsProps> = ({reactions}) => (
    <ul>
        {
            reactions.map((reaction: IReaction) => (<Reaction key={reaction.id} reaction={reaction}/>))
        }
    </ul>
);